$(document).ready(function () {
    chrome.storage.local.get(["list"], function (res) {
        list = res["list"];
        $('#list').html(list);
    });

    $('#clear_btn').click(function () {
        chrome.storage.local.clear();
        $('#list').html("");
    });

    $('#save_btn').click(function () {
        chrome.storage.local.get(["list"], function (res) {
            chrome.tabs.query({ 'active': true, 'lastFocusedWindow': true }, function (tabs) {
                list = res["list"];
                var url = tabs[0].url;
                if (list)
                    list += "<p class=\"link\" style=\"cursor: pointer;max-width: 176px;\">" + url + "</p><div style=\"border-top: 1px solid #e2e2e2; margin: 5px 10px;\"></div>";
                else
                    list = "<p class=\"link\" style=\"cursor: pointer;max-width: 176px;\">" + url + "</p><div style=\"border-top: 1px solid #e2e2e2; margin: 5px 10px;\"></div>";
                $('#list').html(list);
                chrome.storage.local.set({ "list": list })
            });
        });
    });

    $("body").on("click", ".link", function () {
        chrome.tabs.create({ 'url': $(this).text() })
    })
});